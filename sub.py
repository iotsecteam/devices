import paho.mqtt.client as mqtt
import time, hashlib, json, ssl, os, sys

import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)

USERNAME = "device22"
PASSWORD = "12345"
MQTTBROKER = "test.mosquitto.org"
MQTTPORT = 1883
publishTopic = "iotSecurity/"
sendHashAfter = 5 #messages


TRIG = 23
ECHO = 24
GPIO.setup(TRIG,GPIO.OUT)
GPIO.setup(ECHO,GPIO.IN)


def on_connect(client, userdata, flags, rc):
	print("Connected with result code "+str(rc))

def on_message(client, userdata, msg):
	print(msg.topic+" "+str(msg.payload))


#hash code, certificates, keys and cpuinfo
flist = os.listdir("./")
hash_obj = hashlib.sha512(open(flist[0], 'rb').read())
for fname in flist[1:]:
	path = os.path.join("./", fname)
	if os.path.isdir(path):
        	# skip directories
        	continue
	hash_obj.update(open(fname, 'rb').read())
hash_obj.update(open('/proc/cpuinfo','rb').read())

print hash_obj.hexdigest()

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

#client.username_pw_set(USERNAME, password=PASSWORD)
#client.tls_set("ca_new.crt", certfile="client2_new.crt", keyfile="client2.key", cert_reqs=ssl.CERT_REQUIRED,tls_version=ssl.PROTOCOL_TLSv1, ciphers=None)
client.connect(MQTTBROKER, MQTTPORT, 60)
client.loop_start()


loopcount = 1
while True:
	print "Distance Measurement In Progress"
	GPIO.output(TRIG, False)
	print "Waiting For Sensor To Settle"
	time.sleep(2)

	GPIO.output(TRIG, True)
	time.sleep(0.00001)
	GPIO.output(TRIG, False)

	while GPIO.input(ECHO)==0:
		pulse_start = time.time()

	while GPIO.input(ECHO)==1:
		pulse_end = time.time()

	pulse_duration = pulse_end - pulse_start

	distance = pulse_duration * 17150

	distance = round(distance, 2)

	print "Distance:",distance,"cm"

	if loopcount % (sendHashAfter+2) == 0:
		sha256hash = hash_obj.hexdigest()
		msg_payload = json.dumps({'distance':distance, 'sha256': sha256hash})
		loopcount = 1
	else:
		msg_payload = json.dumps({'distance':distance})

	client.publish(publishTopic + USERNAME, payload=msg_payload, qos=0, retain=False)
	loopcount = loopcount + 1
	time.sleep(2)
